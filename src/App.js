import React from 'react';

import './App.css';
import './components/css/Media.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/pages/Home';
import Services from './components/pages/Services';
import Products from './components/pages/Products';
import SignUp from './components/pages/SignUp';
import Letter from './components/pages/Letter';
import Categories from './components/pages/Categories';
import Login from './components/dashboard/Login';
import Dashboard from './components/dashboard/Dashboard';
import RouterWrapper from './components/RouterWrapper';
import RouterDoashboard from './components/RouterDoashboard';

function App() {


  return (
    <>
      <Router>
        <Switch>
          <RouterWrapper>
            <Route path='/' exact component={Home} />
            <Route path='/services' exact component={Services} />
            <Route path='/products' exact component={Products} />
            <Route path='/sign-up' exact component={SignUp} />
            <Route path='/letter/:id' component={Letter} />
            <Route path='/letter' exact component={Letter} />
            <Route path='/categories/:id' component={Categories} />
            <Route path='/categories' exact component={Categories} />
          </RouterWrapper>
        </Switch>
        <Switch>
          <RouterDoashboard>
            <Route path='/login' exact component={Login} />
            <Route path='/dashboard' exact component={Dashboard} />
          </RouterDoashboard>
        </Switch>
      </Router>
    </>
  );
}

export default App;
