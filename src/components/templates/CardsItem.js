import React from 'react'
import { Link } from 'react-router-dom'

function CardsItem(props) {

    return (
        <>
            <div className='col-lg-3 col-md-6 col-12 cards__item' key={props?.id}>
                <Link className='cards__item__link' to={`/${props.path}?id=${props.id}`} >
                    <div className='cards__item__pic-wrap'
                    >
                        <img src={'/' + props.src} alt={props.alt} className='cards__item__img' />
                    </div>
                    <div className='cards__item__info'>
                        <h5 className='cards__item__text'>
                            {props.text}
                        </h5>
                    </div>
                </Link>
            </div>
        </>
    )
}

export default CardsItem
