import React, { useEffect, useState } from 'react'
import { Button } from './Button'
import '../css/HeroSection.css'

function HeroSection() {
    const [animated, setAnimated] = useState('hiden')
    const [video, setVideo] = useState('/videos/login.mp4')
    useEffect(() => {
        if (animated === 'hiden') {
            setTimeout(() => {
                setAnimated('fadeInRight animated')
                setVideo('/videos/video-2.mp4')
            }, 8000)
        }

    }, [animated])

    return (
        <div className='hero-container'>
            <video src={video} autoPlay loop muted />
            <h1 className={animated}>ADVENTURE AWAITS</h1>
            <p className={animated}>What are you waiting for?</p>
            <div className={`hero-btns ${animated}`}>
                <Button
                    className='btns'
                    buttonStyle='btn--outline'
                    buttonSize='btn-large'
                >
                    GET STARTED
                </Button>

                <Button
                    className='btns'
                    buttonStyle='btn--pramiry'
                    buttonSize='btn-large'
                >
                    WATCH TRAILER
                    <i className='fa fa-play-circle' />
                </Button>
            </div>
        </div>
    )
}

export default HeroSection
