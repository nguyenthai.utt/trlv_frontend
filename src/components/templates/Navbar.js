import React, { useState, useEffect } from 'react'
import { API_TRLV } from '../../entity/entity'
import { Link } from 'react-router-dom'
import { Button } from './Button';
import '../css/Navbar.css'

function Narbar() {
    const [click, setClick] = useState(false)
    const [button, setButton] = useState(true)
    const [dataMenu, setDataMenu] = useState([])

    const handClick = () => { setClick(!click); }

    const closeMobileMenu = () => { setClick(false) }

    const showButton = () => {
        if (window.innerWidth <= 960) {
            setButton(false);
        } else {
            setButton(true);
        }
    }

    useEffect(() => {
        showButton()
    }, [])


    //Get data Menu
    useEffect(() => {
        fetch(`${API_TRLV}/menu`)
            .then(res => res.json())
            .then(
                (result) => {
                    setDataMenu(result)
                },
                (error) => {
                    console.log(error);
                }
            )
    }, [])

    window.addEventListener('resize', showButton)

    return (
        <>
            <nav className='navbar'>
                <div className='navbar-container'>
                    <Link to='/' className='navbar-logo'>
                        TRVL <i className='fa fa-heart' />
                    </Link>
                    <div className='menu-icon' onClick={handClick}>
                        <i className={click ? 'fa fa-times' : ' fa fa-bars'} />
                    </div>
                    <ul className={click ? 'nav-menu active' : 'nav-menu'}>
                        {dataMenu && dataMenu.map((item, index) => {
                            return <li className='nav-item' key={index}>
                                <Link to={item.link} className='nav-links' onClick={closeMobileMenu}>
                                    {item.name}
                                </Link>
                            </li>
                        })}
                        {/* <li className='nav-item'>
                            <Link to='/' className='nav-links' onClick={closeMobileMenu}>
                                Home
                            </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/services' className='nav-links' onClick={closeMobileMenu}>
                                Services
                            </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/products' className='nav-links' onClick={closeMobileMenu}>
                                Products
                            </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/sign-up' className='nav-links-mobile' onClick={closeMobileMenu}>
                                Sign Up
                            </Link>
                        </li> */}
                    </ul>

                    {button && <Button buttonStyle='btn--outline'>SIGN UP</Button>}
                </div>
            </nav>
        </>
    )
}

export default Narbar
