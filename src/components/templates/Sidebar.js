import React from 'react'
import { Link } from 'react-router-dom'

function Sidebar(props) {
    const dataSidebar = props.data;

    return (
        <div className="letter-sidebar" key={dataSidebar?.categories?.name}>
            <div className="title-sidebar">
                <h3>
                    <Link to={`/${dataSidebar?.categories?.link}?id=${dataSidebar?.categories?.id}`}>
                        <span>{dataSidebar?.categories?.name}</span>
                    </Link>
                </h3>
                {/* <div className="sidebar-line"></div> */}
            </div>
            <div className="list-letter-sidebar">
                <div className="row">
                    {dataSidebar?.data && dataSidebar?.data?.map(item => {
                        return <div className="col-lg-12 col-md-6 list-sidebar" key={item?.id}>
                            <div className="item-sidebar-box">
                                <Link to={`/${item?.link}?id=${item?.id}`}>
                                    <div className="item-letter-sidebar">
                                        <div className="item-letter-sidebar-image zoom">
                                            <img src={'/' + item?.image} alt={item?.title} />
                                        </div>
                                        <div className="item-letter-sidebar-title">
                                            <h4>{item?.title}</h4>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                    })}
                </div>
            </div>
        </div>
    )
}

export default Sidebar
