import React from 'react'
import CardsItem from './CardsItem'
import '../css/Cards.css'
import { Link } from 'react-router-dom'

function Cards(props) {

    return (
        <div className='cards' key={props?.dataLetter?.categories?.name}>
            <h1>{props?.dataLetter?.categories?.name}</h1>
            <div className='cards__container'>
                <div className='cards__wrapper'>
                    <div className='cards__items row'>
                        {props.dataLetter?.data?.length > 0 && props.dataLetter?.data.map(item => {
                            return <>
                                < CardsItem
                                    id={item?.id}
                                    src={item?.image}
                                    text={item?.title}
                                    // label={item?.categories}
                                    path={item?.link}
                                    alt={item?.title}
                                />
                            </>
                        })}
                    </div>

                    <Link className="read-more" to={`categories/${props?.dataLetter?.categories?.link}?id=${props?.dataLetter?.categories?.id}`}>
                        Read More <i className="fa fa-arrow-right"></i>
                    </Link>
                </div>
            </div>
        </div >
    )
}

export default Cards
