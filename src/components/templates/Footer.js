import React from 'react'
import { Link } from 'react-router-dom'
import { Button } from './Button'
import '../css/Footer.css'

function Footer() {
    return (
        <div className='footer-container'>
            <section className='footer-subscription'>
                <p className='footer-subscription-heading'>
                    Join the Adventure newsletter to receive our best vacation deals
                </p>
                <p className='footer-subscription-text'>
                    You can unsubscribe at any time.
                </p>
                <div className='input-areas'>
                    <form>
                        <input
                            type='email'
                            name='email'
                            placeholder='Your Email'
                            className='footer-input'
                        />
                    </form>
                    <Button buttonStyle='btn--primary'>Subscribe</Button>
                </div>
            </section>
            <div className='footer-links'>
                <div className='footer-link-wrapper'>
                    <div className='footer-link-items'>
                        <h2>About Us</h2>
                        <Link to='/sign-up' >How it works</Link>
                        <Link to='/' >Testimonials</Link>
                        <Link to='/' >Careers</Link>
                        <Link to='/' >Investors</Link>
                        <Link to='/' >Terms of Service</Link>
                    </div>
                    <div className='footer-link-items'>
                        <h2>Contact Us</h2>
                        <Link to='/' >How it works</Link>
                        <Link to='/' >Testimonials</Link>
                        <Link to='/' >Careers</Link>
                        <Link to='/' >Investors</Link>
                        <Link to='/' >Terms of Service</Link>
                    </div>
                </div>

                <div className='footer-link-wrapper'>
                    <div className='footer-link-items'>
                        <h2>Videos</h2>
                        <Link to='/' >How it works</Link>
                        <Link to='/' >Submit Video</Link>
                        <Link to='/' >Ambassadors</Link>
                        <Link to='/' >Agency</Link>
                        <Link to='/' >Influencer</Link>
                    </div>
                    <div className='footer-link-items'>
                        <h2>Social Media</h2>
                        <Link to='/' >Instagram</Link>
                        <Link to='/' >Facebook</Link>
                        <Link to='/' >Youtube</Link>
                        <Link to='/' >Twitter</Link>
                        <Link to='/' >LinkedIn</Link>
                    </div>
                </div>
            </div>
            <section className='social-media'>
                <div className='social-media-wrap'>
                    <div className='footer-logo'>
                        <Link to='/' className='social-logo' >
                            TRLV
                            <i className='fa fa-heart' />
                        </Link>
                    </div>
                    <small className='website-rights'>TRLV @ 2021</small>
                    <div className='social-icons'>
                        <Link to='/' aria-label='Facebook' className='social-icon-link facebook'>
                            <i className='fa fa-facebook' />
                        </Link>
                        <Link to='/' aria-label='Instagram' className='social-icon-link instagram'>
                            <i className='fa fa-instagram' />
                        </Link>
                        <Link to='/' aria-label='Youtube' className='social-icon-link youtube'>
                            <i className='fa fa-youtube' />
                        </Link>
                        <Link to='/' aria-label='Twitter' className='social-icon-link twitter'>
                            <i className='fa fa-twitter' />
                        </Link>
                        <Link to='/' aria-label='LinkedIn' className='social-icon-link linedin'>
                            <i className='fa fa-linkedin' />
                        </Link>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Footer
