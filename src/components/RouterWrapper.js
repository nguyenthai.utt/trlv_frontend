import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import Footer from './templates/Footer';
import Navbar from './templates/Navbar';

function RouterWrapper(props) {
    const history = useHistory();
    useEffect(() => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }, [history.location.pathname])

    let check = false;
    props.children?.forEach(item => {
        if (props.location.pathname === item?.props.path || (item?.props.path !== '/' &&
            props.location.pathname.indexOf(item?.props.path) > -1)) { check = true }
    })

    return (
        <>
            {check && <>
                <Navbar />
                {props.children}
                <Footer />
            </>
            }
        </>
    )

}

export default RouterWrapper
