import React, { useEffect, useState } from 'react'
import '../../App.css'
import Cards from '../templates/Cards';
import HeroSection from '../templates/HeroSection';
import { API_TRLV } from '../../entity/entity'
import Loadding from '../templates/Loadding'

function Home() {
    const [dataLetter, setDataLetter] = useState([])
    const [isLoadding, setLoadding] = useState(false)
    //Get data letter
    useEffect(() => {
        setLoadding(true)
        fetch(`${API_TRLV}/letter`)
            .then(res => res.json())
            .then(
                (result) => {
                    setDataLetter(result)
                    setLoadding(false)
                },
                (error) => {
                    setLoadding(false)
                }
            )
    }, [])
    return (
        <>
            {isLoadding && <Loadding />}
            <HeroSection />
            {dataLetter?.length > 0 && dataLetter.map(item => {
                console.log(item?.title);
                return <Cards dataLetter={item} />
            })}
        </>
    )
}

export default Home;