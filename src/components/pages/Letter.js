import React, { useEffect, useState } from 'react'
import { API_TRLV } from '../../entity/entity'
import '../../App.css'
import Sidebar from '../templates/Sidebar'
import { Link } from 'react-router-dom'
import Loadding from '../templates/Loadding'

function Letter(props) {
    const [dataLetter, setDataLetter] = useState([])
    const [dataCategories, setDataCategories] = useState([])
    const [isLoadding, setLoadding] = useState(false)

    //Get data Menu
    useEffect(() => {
        setLoadding(true)
        fetch(`${API_TRLV}/letterId${props.location?.search}`)
            .then(res => res.json())
            .then(
                (result) => {
                    setDataLetter(result?.letter)
                    setDataCategories(result?.listcategories)
                    setLoadding(false)
                },
                (error) => {
                    setLoadding(false)
                }
            )
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.location?.search])

    return (
        <>
            {isLoadding && <Loadding />}
            <div className="letter">
                <div className="letter-title">
                    <h1>{dataLetter?.title}</h1>
                    <div className="letter-categories">
                        {dataLetter?.categories && dataLetter?.categories.map((item) => {
                            return <Link key={item?.id} to={`/${item?.link}?id=${item?.id}`} className="tag-outline">{item?.categories_id}</Link>
                        })}
                    </div>
                </div>
                <div className="letter-container">
                    <div className="letter-left">
                        <div className="letter-image">
                            <img src={'/' + dataLetter?.image} alt={dataLetter?.title} />
                        </div>
                        <div className="letter-content">
                            <p>{dataLetter?.content}</p>
                        </div>
                    </div>

                    <div className="letter-right">
                        {dataCategories?.length > 0 && dataCategories.map(item => {
                            return <Sidebar data={item} />
                        })}
                    </div>
                </div>
            </div>
        </>
    )
}

export default Letter
