import React, { useEffect, useState } from 'react'
import { API_TRLV } from '../../entity/entity'
import '../../App.css'
import Cards from '../templates/Cards';
import Loadding from '../templates/Loadding';

function Categories(props) {
    const [dataCategories, setDataCategories] = useState([])
    const [isLoadding, setLoadding] = useState(false)

    //Get data Menu
    useEffect(() => {
        setLoadding(true)
        fetch(`${API_TRLV}/categories${props.location?.search}`)
            .then(res => res.json())
            .then(
                (result) => {
                    setDataCategories(result)
                    setLoadding(false)
                },
                (error) => {
                    console.log(error);
                    setLoadding(false)
                }
            )
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return (
        <>
            {isLoadding && <Loadding />}
            <Cards dataLetter={dataCategories} />
        </>
    )
}

export default Categories
